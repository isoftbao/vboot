/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import ModalMixins from './ModalMixins';

/**
 * 对象合并
 * @param target
 * @param fields
 * @param include
 */
const mergeObj = (target, fields = [], include = true) => {
  const newObj = {};
  if (include) {
    for (let i = 0; i < fields; i++) {
      const key = fields[i];
      newObj[key] = target[key];
    }
  } else {
    for (const field in target) {
      if (fields.indexOf(field) === -1) {
        newObj[field] = target[field];
      }
    }
  }
  return newObj;
};

const recursion = (list, data, state) => {
  data.forEach(value => {
    if (state.indexOf(value.checkState) !== -1) {
      list.push(value);
    }
    if (value.children) {
      recursion(list, value.children, state);
    }
  });
};

/**
 * 获取 Easyui 树形checked 数据
 * @param data
 * @param state
 * @returns {Array}
 */
const getChecked = (data = [], state = ['checked']) => {
  const list = [];
  recursion(list, data, state);
  return list;
};

export default {
  ModalMixins,
  mergeObj,
  getChecked,
  baseUrl: (process.env.NODE_ENV === 'development') ? '/server/' : '/'
};
